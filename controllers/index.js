var express = require('express');
var app = express();
var router = express.Router();
var request = require('request');
var multer = require('multer');
var User = require('../models/user');
var User_Confirm = require('../models/user_confirm');
var Manager = require('../models/manager');
var Topic = require('../models/topic');
var Background = require('../models/background');
var Sticker = require('../models/sticker');
var Charater = require('../models/character');

router.route('/')
.get(function(req,res){
	res.render('homepage');

})

router.route('/home')
.get(function(req,res){
	var autistId = req.session._id;
	Topic.find({'artistId' : autistId},function(err, topic){
		if(err){
			res.json({message: 'err'});
		}else{
			var end = JSON.stringify(topic);
			res.render('home', {topic: JSON.parse(end)});
		}
	})
})

router.route('/homemanager')
.get(function(req,res){
	res.render('home_manager');
})

router.route('/addTopic')
.post(function(req,res){
	var newTopic = new Topic({
		title : req.body.title,
		category : req.body.category,
		artistId : req.session._id,
		status : 0
	});
	newTopic.save(function(error){
		if(error){
			console.log(error);
		}else{
			res.redirect('/home');
		}
	})
})

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/img')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
})

var upload = multer({ storage: storage });

//Thêm sticker
router.post('/addSticker',upload.array("file",12),function(req,res){
	var chk = true;
	var arr = req.files;
	var topicId = req.body.topicId;
	var category = req.body.category;
	if(category == 'Background'){
		for (var i = 0; i < arr.length; i++) {
			var newSticker = new Background({
				url : "/img/"+ arr[i].originalname,
				topicId : topicId
			});
			newSticker.save(function(error){
				if(error){
					console.log(error);
					chk = false;
				}
			})
		}
		if(chk){
			res.redirect('/home');
		}
	}else{
		if(category == 'Sticker'){
			for (var i = 0; i < arr.length; i++) {
				var newSticker = new Sticker({
					url : "/img/"+ arr[i].originalname,
					topicId : topicId
				});
				newSticker.save(function(error){
					if(error){
						console.log(error);
						chk = false;
					}
				})
			}
			if(chk){
				res.redirect('/home');
			}
		}else{
			for (var i = 0; i < arr.length; i++) {
				var newSticker = new Charater({
					url : "/img/"+ arr[i].originalname,
					topicId : topicId
				});
				newSticker.save(function(error){
					if(error){
						console.log(error);
						chk = false;
					}
				})
			}
			if(chk){
				res.redirect('/home');
			}
		}
	}
	
})

router.post('/signup',function(req,res){
	var newUser = new User({
		email : req.body.email,
		password : req.body.password
	});
	console.log(newUser);
	newUser.save(function(error){
		if (error) {
			console.log(error);
		}else{
			res.redirect('/');
		}
	})
});


router.post('/login',function(req,res){
	req.session.chklog = 0;
	User.findOne({'email':req.body.email,'password':req.body.password})
	.exec(function(err, value){
		if (err) {
			alert('Loi dang nhap');
		}else{
			if(value != null){
				req.session.chklog = '1';
				req.session.email = req.body.email;		
				req.session._id = value._id;			
				res.redirect('/home');
			}else{
				User_Confirm.findOne({'email':req.body.email,'password':req.body.password})
				.exec(function(err,value){
					if(err){
						alert('Lỗi đăng nhập');
					}else{
						if(value != null){
							req.session.chklog = 2;
							req.session.email = req.body.email;		
							req.session._id = value._id;			
							res.redirect('/homemanager');
						}else{
							Manager.findOne({'email':req.body.email,'password':req.body.password})
							.exec(function(err,value){
								if(err){
									alert('Lỗi đăng nhập');
								}else{
									if(value != null){
										req.session.chklog = 3;
										req.session.email = req.body.email;		
										req.session._id = value._id;			
										res.redirect('/admin');
									}else{
										res.redirect('/');
									}
								}
							})
						}
					}
				})
			}
		}
	})
})

router.route('/logout')
.get(function(req,res){
  req.session.destroy();
  res.redirect('/');
})

module.exports = router