var express = require('express');
var app = express();
var router = express.Router();
var request = require('request');
var User = require('../models/user');
var User_Confirm = require('../models/user_confirm');
var ObjectID = require('mongodb').ObjectID;

router.route('/')
.get(function(req,res){
	res.render('./admin/index');
})

router.route('/User_Artist')
.get(function(req,res){
	var option = {
		url: "http://localhost:3000/api/ListUser",
		method: "get"
	};
	request(option,function(error, response, list_user){
		if(!error && response.statusCode == 200){
			res.render('./admin/user_autist', {list_user: JSON.parse(list_user).data})
		}
	})
})

router.route('/User_Confirm')
.get(function(req,res){
	var option = {
		url: "http://localhost:3000/api/List_User_Confirm",
		method: 'get'
	};
	request(option,function(error, response,list_user){
		if(!error && response.statusCode == 200){
			res.render('./admin/user_confirm', {list_user: JSON.parse(list_user).data})
		}
	})
})

router.post('/AddUserAutist',function(req,res){
	var newUser = new User({
		email : req.body.email,
		password : req.body.password
	});
	console.log(newUser);
	newUser.save(function(error){
		if (error) {
			console.log(error);
		}else{
			res.redirect('/admin/User_Artist');
		}
	})
});

router.post('/AddUserConfirm',function(req,res){
	var newUser = new User_Confirm({
		email : req.body.email,
		password : req.body.password
	});
	console.log(newUser);
	newUser.save(function(error){
		if (error) {
			console.log(error);
		}else{
			res.redirect('/admin/User_Confirm');
		}
	})
});

router.post('/EditUserAutist',function(req,res){
	var user_id = req.body.user_id;
	var email = req.body.email;
	var password = req.body.password;
	User.update({'_id':ObjectID(user_id)}, {'email':email,'password':password},function(err,result){
		if(err){
			res.JSON({message : 'err'})
		}else{
			res.redirect('/admin/User_Artist');
		}
	})
})

router.post('/EditUserConfirm',function(req,res){
	var user_id = req.body.user_id;
	var email = req.body.email;
	var password = req.body.password;
	User_Confirm.update({'_id':ObjectID(user_id)}, {'email':email,'password':password},function(err,result){
		if(err){
			res.JSON({message : 'err'})
		}else{
			res.redirect('/admin/User_Confirm');
		}
	})
})

router.post('/delete_user_autist',function(req,res){
	var user_id = req.body.user_id;
	console.log("delete");
	User.deleteOne({'_id':ObjectID(user_id)}, function(err,result){
		if(err){
			res.JSON({message : 'err'})
		}else{
			res.redirect('/admin/User_Artist');
		}
	})
})

router.post('/delete_user_confirm',function(req,res){
	var user_id = req.body.user_id;
	console.log("delete");
	User_Confirm.deleteOne({'_id':ObjectID(user_id)}, function(err,result){
		if(err){
			res.JSON({message : 'err'})
		}else{
			res.redirect('/admin/User_Confirm');
		}
	})
})
module.exports = router