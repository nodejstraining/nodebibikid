var express = require('express');
var app = express();
var router = express.Router();
var Topic = require('../models/topic');
var Background = require('../models/background');
var Sticker = require('../models/sticker');
var Character = require('../models/character');
var ObjectID = require('mongodb').ObjectID;
var request = require('request');
var User = require('../models/user');
var User_Confirm = require('../models/user_confirm');
//Lấy tất cả user
router.route('/ListUser')
.get(function(req,res){
	User.find()
	.exec(function(err, list_user){
		if(err){
			res.json({message:'error'})
		}else{
			res.json({data : list_user});
		}
	})
})

router.route('/List_User_Confirm')
.get(function(req,res){
	User_Confirm.find()
	.exec(function(err, list_user){
		if(err){
			res.json({message:'error'})
		}else{
			res.json({data : list_user});
		}
	})
})



//Update status of topic is ok
router.route('/confirm')
.get(function(req,res){
	var topicId = req.query.topicId;
	Topic.update({'_id': ObjectID(topicId)},{'status':1},function(err,result){
		if(err){
			res.JSON({message : 'err'})
		}else{
			res.send(JSON.stringify(result))
		}
	})
})

//Update status of topic is warning
router.route('/warning')
.get(function(req,res){
	var topicId = req.query.topicId;
	Topic.update({'_id': ObjectID(topicId)},{'status':2},function(err,result){
		if(err){
			res.JSON({message : 'err'})
		}else{
			res.send(JSON.stringify(result))
		}
	})
})

//Update status of topic is waitting
router.route('/waitting')
.get(function(req,res){
	var topicId = req.query.topicId;
	Topic.update({'_id': ObjectID(topicId)},{'status':0},function(err,result){
		if(err){
			res.JSON({message : 'err'})
		}else{
			res.send(JSON.stringify(result))
		}
	})
})


router.route('/confirm_topic_background')
.get(function(req,res){
	var category = req.body.category;
	var topicbackground = []
	Topic.find({$and:[{'status': 0}, {'category': "Background"}]},
		{'_id': 1, 'title': 1, 'status': 1})
	.then(function(topic_background){
		var Background_image = [];
		topicbackground = topic_background;
		topic_background.forEach(function(topic_id){
			Background_image.push(
				Background.find({'topicId': topic_id._id})
					.exec()
			)
		})
		return Promise.all(Background_image);	
	})
	.then(function(Return){
		Topic_Background = [];
		for(var index = 0; index < topicbackground.length; index++){
			Topic_Background[index] = {
				idTopic : topicbackground[index]._id,
				title : topicbackground[index].title,
				status: topicbackground[index].status,
				background: Return[index]
			}
		}
		res.send(JSON.stringify(Topic_Background));
	})
	.catch(function(err){
		throw err;
	})
		
	
})

router.route('/confirm_topic_character')
.get(function(req,res){
	var category = req.body.category;
	var topiccharacter = []
	Topic.find({$and:[{'status': 0}, {'category': "Character"}]},
		{'_id': 1, 'title': 1, 'status': 1})
	.then(function(topic_character){
		var Character_image = [];
		topiccharacter = topic_character;
		topic_character.forEach(function(topic_id){
			Character_image.push(
				Character.find({'topicId': topic_id._id})
					.exec()
			)
		})
		return Promise.all(Character_image);	
	})
	.then(function(Return){
		Topic_Character = [];
		for(var index = 0; index < topiccharacter.length; index++){
			Topic_Character[index] = {
				idTopic : topiccharacter[index]._id,
				title : topiccharacter[index].title,
				status: topiccharacter[index].status,
				character: Return[index]
			}
		}
		res.send(JSON.stringify(Topic_Character));
	})
	.catch(function(err){
		throw err;
	})
		
	
})

router.route('/confirm_topic_sticker')
.get(function(req,res){
	var category = req.body.category;
	var topicsticker = []
	Topic.find({$and:[{'status': 0}, {'category': "Sticker"}]},
		{'_id': 1, 'title': 1, 'status': 1})
	.then(function(topic_sticker){
		var Sticker_image = [];
		topicsticker = topic_sticker;
		topic_sticker.forEach(function(topic_id){
			Sticker_image.push(
				Sticker.find({'topicId': topic_id._id})
					.exec()
			)
		})
		return Promise.all(Sticker_image);	
	})
	.then(function(Return){
		Topic_Sticker = [];
		for(var index = 0; index < topicsticker.length; index++){
			Topic_Sticker[index] = {
				idTopic : topicsticker[index]._id,
				title : topicsticker[index].title,
				status: topicsticker[index].status,
				sticker: Return[index]
			}
		}
		res.send(JSON.stringify(Topic_Sticker));
	})
	.catch(function(err){
		throw err;
	})
		
	
})

//Lấy background theo idtopic

router.route('/backgroundtopic')
.get(function(req,res){
	var topicId = req.query.topicId;
	Background.find({'topicId' : topicId}, function(err,data){
		if(err){
			res.json({message:'err'});
		}else{
			res.send(JSON.stringify(data));
		
		}
	})
})

router.route('/charactertopic')
.get(function(req,res){
	var topicId = req.query.topicId;
	Character.find({'topicId' : topicId}, function(err,data){
		if(err){
			res.json({message:'err'});
		}else{
			res.send(JSON.stringify(data));
		
		}
	})
})

router.route('/stickertopic')
.get(function(req,res){
	var topicId = req.query.topicId;
	Sticker.find({'topicId' : topicId}, function(err,data){
		if(err){
			res.json({message:'err'});
		}else{
			res.send(JSON.stringify(data));
			
		}
	})
})

//Lay hình ảnh theo topic va category
router.route('/topiccategory')
.post(function(req,res){
	var topicId = req.body.topicId;
	var category = req.body.category;
	if(category == 'Background'){
		Background.find({'topicId' : topicId},function(err, topic){
			if(err){
				res.json({message: 'err'});
			}else{	
				
				res.send(JSON.stringify(topic));
					
			}
		})
	}else{
		if(category == 'Sticker'){
			Sticker.find({'topicId' : topicId},function(err, topic){
				if(err){
					res.json({message: 'err'});
				}else{
					Sticker.count({},function(err,counter){
						res.send(JSON.stringify(topic));
					})
				}
			})
		}else{
			Character.find({'topicId' : topicId},function(err, topic){
				if(err){
					res.json({message: 'err'});
				}else{
					Character.count({},function(err,counter){
						res.send(JSON.stringify(topic));
					})
				}
			})
		}
	}
})

//Lấy tất cả Background theo artistID
router.get('/background', function(req, res)
{
	var topicbackgroundabc = []
	Topic.find({$and:[{'artistId': req.query.artistId}, {'category': "Background"}]},
		{'_id': 1, 'title': 1, 'status': 1})
	.then(function(topic_background)
	{
		var Background_image = []
		topicbackgroundabc = topic_background;
		topic_background.forEach(function(topic_id)
		{	
			Background_image.push(
				Background.find({'topicId': topic_id._id})
					.exec()
			)
		})

		return Promise.all(Background_image)
	})
	.then(function(Return)
	{
		var pos = parseInt(Return.length)
		var Topic_Background = [];

		for(var index = 0; index < topicbackgroundabc.length; index++)
		{
			Topic_Background[index] = {
				title : topicbackgroundabc[index].title,
				status: topicbackgroundabc[index].status,
				background: Return[index]
			}
		}
		res.send(JSON.stringify(Topic_Background))	
		
	})
	.catch(function(err){
		throw err;
	})
})

router.get('/character', function(req, res)
{
	var topiccharacter = []
	Topic.find({$and:[{'artistId': req.query.artistId}, {'category': "Character"}]},
		{'_id': 1, 'title': 1, 'status': 1})
	.then(function(topic_character)
	{
		var Character_image = []
		topiccharacter = topic_character;
		topic_character.forEach(function(topic_id)
		{	
			Character_image.push(
				Character.find({'topicId': topic_id._id})
					.exec()
			)
		})

		return Promise.all(Character_image)
	})
	.then(function(Return)
	{
		var pos = parseInt(Return.length)
		var Topic_Character= [];

		for(var index = 0; index < topiccharacter.length; index++)
		{
			Topic_Character[index] = {
				title : topiccharacter[index].title,
				status: topiccharacter[index].status,
				character: Return[index]
			}
		}
		res.send(JSON.stringify(Topic_Character))	
		
	})
	.catch(function(err){
		throw err;
	})
})

router.get('/sticker', function(req, res)
{
	var topicsticker = []
	Topic.find({$and:[{'artistId': req.query.artistId}, {'category': "Sticker"}]},
		{'_id': 1, 'title': 1, 'status': 1})
	.then(function(topic_sticker)
	{
		var Sticker_image = []
		topicsticker = topic_sticker;
		topic_sticker.forEach(function(topic_id)
		{	
			Sticker_image.push(
				Sticker.find({'topicId': topic_id._id})
					.exec()
			)
		})

		return Promise.all(Sticker_image)
	})
	.then(function(Return)
	{
		var pos = parseInt(Return.length)
		var Topic_Sticker= [];

		for(var index = 0; index < topicsticker.length; index++)
		{
			Topic_Sticker[index] = {
				title : topicsticker[index].title,
				status: topicsticker[index].status,
				sticker: Return[index]
			}
		}
		res.send(JSON.stringify(Topic_Sticker))	
		
	})
	.catch(function(err){
		throw err;
	})
})
module.exports = router;