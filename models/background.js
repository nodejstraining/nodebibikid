var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var Schema_Background = new Schema({
	topicId: {
		type: mongoose.Schema.Types.ObjectId,
		ref : 'Topic'
	} ,
	url : String,
})

module.exports = mongoose.model('Background', Schema_Background);
