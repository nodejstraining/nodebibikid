var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Schema_Manager = new Schema({
	email : String,
	username : String,
	password : String,
	created_at : Date,
	updated_at : Date
});

Schema_Manager.pre('save',function(next){
	var cur = new Date().toISOString()
	this.updated_at = cur;
	if(!this.created_at){
		this.created_at = cur;
		next();
	}
});

module.exports = mongoose.model('Manager', Schema_Manager);