var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Schema_User_Confirm = new Schema({
	email : String,
	username : String,
	password : String,
	created_at : Date,
	updated_at : Date
});

Schema_User_Confirm.pre('save',function(next){
	var cur = new Date().toISOString()
	this.updated_at = cur;
	if(!this.created_at){
		this.created_at = cur;
		next();
	}
});

module.exports = mongoose.model('User_Confirm', Schema_User_Confirm);