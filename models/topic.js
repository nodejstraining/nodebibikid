//Đây là nơi lưu trữ bộ sticker theo từng category
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var Schema_Topic = new Schema({
	title : String, //Tiêu đề của bộ Sticker
	artistId :{
		 type: mongoose.Schema.Types.ObjectId,
	     ref : 'User' //id của tác giả upload nên
	},
	status : Number, // 0-waitting, 1: confirm, 2: cảnh báo
	category : String
});

module.exports = mongoose.model('Topic', Schema_Topic);