function ShowTopic(id, category, status,title){
  $.ajax({
    url:'./api/topiccategory',
    type:'POST',
    data:{topicId:id, category:category},
    success:function(data){
      var show = '<div class="col-md-5 col-md-offset-3" style="padding: 20px">'+
      '<div class="thumbnail" style="min-height: 200px">'+
      '<div><span class="glyphicon glyphicon-pencil" style="float:right;" ></span></div>'+
      '<div class="img-responsive" style="min-height: 150px">';      
      hu = JSON.parse(data);

      if (hu != ''){
        for(i in hu){
          show +=  '<img  src="'+ hu[i].url +'" style="width: 58px; height: 60px; margin: 10px;">';
        }
        
      }else{
        show += '<p>Chưa có dữ liệu</p>';
      } 
      if(status == 1){
       show += '</div>'+
       '<div class="caption">'+
       '<div class="clearfix">'+
       '<div class="title pull-left">'+title+'</div>'+                     
       '<a  class="btn btn-success pull-right" ><span class="glyphicon glyphicon-ok"></span></a>'+
       '</div>'+
       '</div>'+
       '</div>'+
       '</div>';
     }else{
      if(status == 0){
              show += '</div>'+
              '<div class="caption">'+
              '<div class="clearfix">'+
              '<div class="title pull-left">'+title+'</div>'+                     
              '<a  class="btn btn-info pull-right" >Waiting</a>'+
              '</div>'+
              '</div>'+
              '</div>'+
              '</div>';
            }else{
                show += '</div>'+
              '<div class="caption">'+
              '<div class="clearfix">'+
              '<div class="title pull-left">'+title+'</div>'+                     
              '<a  class="btn btn-warning pull-right" >Warning</a>'+
              '</div>'+
              '</div>'+
              '</div>'+
              '</div>';
            }
    }        
    document.getElementById('show_noidung').innerHTML = show;
  }
})
  
}
function Upload(id, category, title){
	document.getElementById('chan1').innerHTML = 'Upload to ' + title;
	document.getElementById('chan2').value = category;
	document.getElementById('chan3').value = id;
}

// Content_page

function Background(id){
  $.ajax({
    url:'./api/background',
    type:'GET',
    data:{artistId:id},
    success:function(data){
      var he = JSON.parse(data)
      var total = '';
      if(he != ''){
        var show ='';
        for(i in he){

         if(he[i].background != ''){
          var show = '<div class="col-sm-6 col-md-5" style="margin-left :50px; padding: 20px">'+
          '<div class="thumbnail" style="min-height: 200px">'+
          '<div class="img-responsive" style="min-height: 150px">'; 
          
          for(j in he[i].background){
            show +=  '<img  id="'+he[i].background[j]._id+'" src="'+ he[i].background[j].url +'" style="width: 58px; height: 60px; margin: 10px;">';
          }
          
          if(he[i].status == 1){
           show += '</div>'+
           '<div class="caption">'+
           '<div class="clearfix">'+
           '<div class="title pull-left">'+he[i].title+'</div>'+                       
           '<a  class="btn btn-success pull-right" ><span class="glyphicon glyphicon-ok"></span></a>'+
           '</div>'+
           '</div>'+
           '</div>'+
           '</div>';
         }else{
          if(he[i].status == 0){
              show += '</div>'+
              '<div class="caption">'+
              '<div class="clearfix">'+
              '<div class="title pull-left">'+he[i].title+'</div>'+                     
              '<a  class="btn btn-info pull-right" >Waiting</a>'+
              '</div>'+
              '</div>'+
              '</div>'+
              '</div>';
            }else{
                show += '</div>'+
              '<div class="caption">'+
              '<div class="clearfix">'+
              '<div class="title pull-left">'+he[i].title+'</div>'+                     
              '<a  class="btn btn-warning pull-right" >Warning</a>'+
              '</div>'+
              '</div>'+
              '</div>'+
              '</div>';
            }
        }  total += show;  }else{
          show += '';
        }
        

      }
      document.getElementById('noidung').innerHTML = total;
    }
  }
})
}


function Character(id){
 $.ajax({
  url:'./api/character',
  type:'GET',
  data:{artistId:id},
  success:function(data){
    var he = JSON.parse(data)
    var total = '';
    if(he != ''){
      var show ='';
      for(i in he){
        if(he[i].character != ''){
          var show = '<div class="col-sm-6 col-md-5" style="margin-left :50px; padding: 20px">'+
          '<div class="thumbnail" style="min-height: 200px">'+
          '<div class="img-responsive" style="min-height: 150px">'; 
          
          for(j in he[i].character){
            show +=  '<img src="'+ he[i].character[j].url +'" style="width: 58px; height: 60px; margin: 10px;">';
          }
          
          if(he[i].status == 1){
           show += '</div>'+
           '<div class="caption">'+
           '<div class="clearfix">'+
           '<div class="title pull-left">'+he[i].title+'</div>'+                       
           '<a  class="btn btn-success pull-right" ><span class="glyphicon glyphicon-ok"></span></a>'+
           '</div>'+
           '</div>'+
           '</div>'+
           '</div>';
         }else{
          if(he[i].status == 0){
              show += '</div>'+
              '<div class="caption">'+
              '<div class="clearfix">'+
              '<div class="title pull-left">'+he[i].title+'</div>'+                     
              '<a  class="btn btn-info pull-right" >Waiting</a>'+
              '</div>'+
              '</div>'+
              '</div>'+
              '</div>';
            }else{
                show += '</div>'+
              '<div class="caption">'+
              '<div class="clearfix">'+
              '<div class="title pull-left">'+he[i].title+'</div>'+                     
              '<a  class="btn btn-warning pull-right" >Warning</a>'+
              '</div>'+
              '</div>'+
              '</div>'+
              '</div>';
            }
        } 
        total += show; 
      }else{
        show += ''
      }
      

    }
    document.getElementById('noidung').innerHTML = total;
  }
}
})
}
function Sticker(id){
	$.ajax({
    url:'./api/sticker',
    type:'GET',
    data:{artistId:id},
    success:function(data){
      var he = JSON.parse(data)
      var total = '';
      if(he != null){
        var show = '';
        for(i in he){
         if(he[i].background != ''){
          var show = '<div class="col-sm-6 col-md-5" style="margin-left :50px; padding: 20px">'+
          '<div class="thumbnail" style="min-height: 200px">'+
          '<div class="img-responsive"  style="min-height: 150px">'; 
          
          for(j in he[i].sticker){
            show +=  '<img src="'+ he[i].sticker[j].url +'" style="width: 58px; height: 60px; margin: 10px;">';
          }
          
          if(he[i].status == 1){
           show += '</div>'+
           '<div class="caption">'+
           '<div class="clearfix">'+
           '<div class="title pull-left">'+he[i].title+'</div>'+                       
           '<a  class="btn btn-success pull-right" ><span class="glyphicon glyphicon-ok"></span></a>'+
           '</div>'+
           '</div>'+
           '</div>'+
           '</div>';
         }else{
          if(he[i].status == 0){
              show += '</div>'+
              '<div class="caption">'+
              '<div class="clearfix">'+
              '<div class="title pull-left">'+he[i].title+'</div>'+                     
              '<a  class="btn btn-info pull-right" >Waiting</a>'+
              '</div>'+
              '</div>'+
              '</div>'+
              '</div>';
            }else{
                show += '</div>'+
              '<div class="caption">'+
              '<div class="clearfix">'+
              '<div class="title pull-left">'+he[i].title+'</div>'+                     
              '<a  class="btn btn-warning pull-right" >Warning</a>'+
              '</div>'+
              '</div>'+
              '</div>'+
              '</div>';
            }
        } total+=show }else{
          show += '<span>Chưa có hình ảnh nào</span>'
        }
        

      }
      document.getElementById('noidung').innerHTML = total;
    }
  }
})
}