var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var path = require('path');
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');//dong bo trong moogoose
var http = require('http').Server(app);
var bodyParser = require('body-parser');
var session = require('express-session');

var configDB = require('./models/database');
mongoose.connect(configDB.url);

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname,'views'));
app.use(express.static(__dirname + '/public'))
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(session({
  secret: 'secret',
  resave: false,
  saveUninitialized: true,
  cookie: {maxAge:180*60*1000}
}))
app.use(function(req,res,next){
	res.locals.session = req.session;
	next();
});

var index = require('./controllers/index');
var api = require('./controllers/api');
var admin = require('./controllers/admin');

app.use('/', index);
app.use('/api',api);
app.use('/admin',admin);

http.listen(port,function(){
	console.log('Server running port '+port);
})